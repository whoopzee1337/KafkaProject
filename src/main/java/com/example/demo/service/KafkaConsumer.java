package com.example.demo.service;


import com.example.demo.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaConsumer {

    @KafkaListener(topics = "kafkaTopic1", groupId = "first", containerFactory = "messageConcurrentKafkaListenerContainerFactory")
    public void listenMessage(Message message) {
        log.info("Kafka consumer received message: id = {}, content = {}", message.getId(), message.getContent());
    }
}
