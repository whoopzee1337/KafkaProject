package com.example.demo.controller;

import com.example.demo.Message;
import com.example.demo.service.KafkaProducer;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class MessageController {

    private KafkaProducer kafkaProducer;

    @PostMapping("sendMessage")
    public void sendMessage(@RequestBody Message msg) {
        kafkaProducer.sendMessage(msg);
    }

}
