package com.example.demo;

import lombok.Data;

@Data
public class Message {

    private int id;
    private String content;
}
